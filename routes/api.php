<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

//Configuracion de cabeceras
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, authorization, content-type");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::delete('logout', 'ApiController@logout');
    Route::get('user', 'ApiController@getAuthUser');

    /*Routes for Hotel*/
    Route::get('hotels', 'HotelController@index');
    Route::post('hotels', 'HotelController@store');
    Route::post('activeHotel', 'HotelController@active');
    Route::get('hotel/{id}', 'HotelController@show');
    Route::put('hotels/{id}', 'HotelController@update');

    /*Routes for Room*/
    Route::get('rooms/{id}', 'RoomController@index');
    Route::post('room', 'RoomController@store');
    Route::post('activeRoom', 'RoomController@active');
    Route::get('getRoom/{id}', 'RoomController@show');
    Route::put('updateRoom/{id}', 'RoomController@update');
    Route::post('searchRoom', 'RoomController@searchRoom');

    /*Routes for Reserve*/
    Route::post('createReserve', 'ReserveController@store');
    Route::get('reserves', 'ReserveController@index');
    Route::get('getReserve/{id}', 'ReserveController@show');
});
