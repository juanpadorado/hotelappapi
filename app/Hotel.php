<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    const CREATED_AT = 'fec_creacion';
    const UPDATED_AT = 'fec_actualiza';

    protected $table = 'hotel';
    protected $primaryKey = 'cod_hotel';

    protected $fillable = [
        'nombre', 'estado', 'ciudad', 'cant_pisos'
    ];

    public function rooms()
    {
        return $this->hasMany('App\Room', 'cod_hotel', 'cod_hotel');
    }
}
