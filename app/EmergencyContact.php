<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmergencyContact extends Model
{
    const CREATED_AT = 'fec_creacion';
    const UPDATED_AT = 'fec_actualiza';

    protected $table = 'contacto_emergencia';
    protected $primaryKey = 'cod_contacto';

    protected $fillable = [
        'nom_completo', 'telefono', 'cod_reserva'
    ];

    public function reserve()
    {
        return $this->belongsTo('App\Reserve', 'cod_reserva');
    }
}
