<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    const CREATED_AT = 'fec_creacion';
    const UPDATED_AT = 'fec_actualiza';

    protected $table = 'reserva';
    protected $primaryKey = 'cod_reserva';

    protected $fillable = [
        'fecha_entrada', 'fecha_salida', 'cant_personas', 'estado', 'cod_habitacion'
    ];

    public function room()
    {
        return $this->belongsTo('App\Room', 'cod_habitacion');
    }

    public function emergencyContacts()
    {
        return $this->hasMany('App\EmergencyContact', 'cod_reserva', 'cod_reserva');
    }

    public function reserveGuests()
    {
        return $this->hasMany('App\ReserveGuest', 'cod_reserva', 'cod_reserva');
    }
}
