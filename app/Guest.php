<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    const CREATED_AT = 'fec_creacion';
    const UPDATED_AT = 'fec_actualiza';

    protected $table = 'huesped';
    protected $primaryKey = 'cod_huesped';

    protected $fillable = [
        'nombre', 'apellido', 'fecha_nacimiento', 'genero', 'tipo_documento', 'num_documento', 'email', 'telefono'
    ];

    public function reserveGuests()
    {
        return $this->hasMany('App\ReserveGuest', 'cod_huesped', 'cod_huesped');
    }
}
