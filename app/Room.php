<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    const CREATED_AT = 'fec_creacion';
    const UPDATED_AT = 'fec_actualiza';

    protected $table = 'habitacion';
    protected $primaryKey = 'cod_habitacion';

    protected $fillable = [
        'identificador', 'estado', 'costo_base', 'impuesto', 'tipo_habitacion', 'ubicacion', 'cant_personas', 'cod_hotel'
    ];

    public function hotel()
    {
        return $this->belongsTo('App\Hotel', 'cod_hotel');
    }
    public function reserves()
    {
        return $this->hasMany('App\Reserve', 'cod_habitacion', 'cod_habitacion');
    }
}
