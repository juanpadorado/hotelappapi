<?php

namespace App\Http\Controllers;

use App\EmergencyContact;
use App\Guest;
use App\Hotel;
use App\Reserve;
use App\ReserveGuest;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use JWTAuth;

class ReserveController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {
        $reserves = Reserve::all();

        return response()->json([
            'success' => true,
            'reserves' => $reserves
        ]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $dataPersons = $request->persons;
            $dataReserve = $request->reserve;

            $reserve                   = new Reserve();
            $reserve->fecha_entrada    = $dataReserve['fecha_entrada'];
            $reserve->fecha_salida     = $dataReserve['fecha_salida'];
            $reserve->cant_personas    = $dataReserve['cant_personas'];
            $reserve->estado           = 'pendiente';
            $reserve->cod_habitacion   = $dataReserve['cod_habitacion'];

            if ($reserve->save()) {

                $emergencyContact = new EmergencyContact();
                $emergencyContact->nom_completo = $request->contacto_nombreCom;
                $emergencyContact->telefono = $request->contacto_telefono;
                $emergencyContact->cod_reserva = $reserve->cod_reserva;

                $emergencyContact->save();

                foreach ($dataPersons as $key => $value) {

                    $guest = Guest::where('tipo_documento', $value['tipo_documento'])
                                  ->where('num_documento', $value['num_documento'])
                                  ->first();

                    if(!isset($guest)){
                        $guest = new Guest();
                        $guest->nombre = $value['nombre'];
                        $guest->apellido = $value['apellido'];
                        $guest->fecha_nacimiento = explode('T', $value['fecha_nacimiento'])[0];
                        $guest->genero = $value['genero'];
                        $guest->tipo_documento = $value['tipo_documento'];
                        $guest->num_documento = $value['num_documento'];
                        $guest->email = $value['email'];
                        $guest->telefono = $value['telefono'];

                        $guest->save();
                    }

                    if($guest) {
                        $reserveGuest = new ReserveGuest();
                        $reserveGuest->cod_reserva = $reserve->cod_reserva;
                        $reserveGuest->cod_huesped = $guest->cod_huesped;
                        $reserveGuest->propietario = ($key == 0) ? 1 : 0;

                        $reserveGuest->save();
                    }
                }

                DB::commit();
                return response()->json([
                    'success' => true,
                    'Hotel'   => $reserve,
                    'message' => 'La reserva se ha creado con éxito. Se enviará la confirmación al correo electrónico'
                ]);
            } else {
                throw new \InvalidArgumentException('La reserva no se pudo crear');
            }
        }catch (\InvalidArgumentException $ex){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }catch (\Exception $ex){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }
    }

    public function show($id)
    {
        $reserve = Reserve::find($id);
        $room = $reserve->room;
        $reserve->room->hotel;
        $emergencyContacts = $reserve->emergencyContacts[0];

        $guests = DB::select(
            "select hs.* 
              from reserva_huesped rshu, 
                   huesped hs
             where hs.cod_huesped = rshu.cod_huesped
               and rshu.cod_reserva = :cod_reserva;", ['cod_reserva' => $id]);

        return response()->json([
            'success' => true,
            'room' => $room,
            'emergencyContacts' => $emergencyContacts,
            'guests' => $guests,
        ]);
    }
}
