<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use JWTAuth;

class RoomController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index($id)
    {
        $hotel = Hotel::find($id);

        return response()->json([
            'success' => true,
            'hotel' => $hotel,
            'rooms' => $hotel->rooms
        ]);
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'identificador'     => 'required',
                'costo_base'        => 'required|integer',
                'impuesto'          => 'required|integer',
                'tipo_habitacion'   => 'required',
                'ubicacion'         => 'required',
                'cant_personas'     => 'required|integer',
                'cod_hotel'         => 'required|integer',
            ]);

            $room                   = new Room();
            $room->identificador    = $request->identificador;
            $room->estado           = 1;
            $room->costo_base       = $request->costo_base;
            $room->impuesto         = $request->impuesto;
            $room->tipo_habitacion  = $request->tipo_habitacion;
            $room->ubicacion        = $request->ubicacion;
            $room->cant_personas    = $request->cant_personas;
            $room->cod_hotel        = $request->cod_hotel;

            if ($room->save()) {
                return response()->json([
                    'success' => true,
                    'Hotel'   => $room,
                    'message' => 'La habitación se ha creado con éxito'
                ]);
            } else {
                throw new \InvalidArgumentException('La habitación no se pudo crear');
            }
        }catch (\InvalidArgumentException $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }catch (ValidationException $ex ) {
            return response()->json([
                'success' => false,
                'message' => $ex->errors()
            ], 400);
        }catch (\Exception $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }
    }

    public function active(Request $request)
    {
        try {
            $room = Room::find($request->get('cod_habitacion'));

            $room->estado = $request->get('estado');

            if ($room->save()) {
                return response()->json([
                    'success' => true,
                    'message' => 'El estado de la habitación se ha actualizado con éxito'
                ]);
            } else {
                throw new \InvalidArgumentException('La habitación no se pudo actualiza');
            }
        }catch (\InvalidArgumentException $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }catch (\Exception $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $room = Room::find($id);

            $room->identificador     = $request->identificador;
            $room->costo_base        = $request->costo_base;
            $room->impuesto          = $request->impuesto;
            $room->tipo_habitacion   = $request->tipo_habitacion;
            $room->ubicacion         = $request->ubicacion;
            $room->cant_personas     = $request->cant_personas;

            if ($room->save()) {
                return response()->json([
                    'success' => true,
                    'room'   => $room,
                    'message' => 'La habitación se ha actualizado con éxito'
                ]);
            } else {
                throw new \InvalidArgumentException('La habitación no se pudo actualiza');
            }
        }catch (\InvalidArgumentException $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }catch (\Exception $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }
    }

    public function show($id)
    {
        $room = Room::find($id);

        return response()->json([
            'success' => true,
            'room' => $room
        ]);
    }

    public function searchRoom(Request $request)
    {
        try {
            $rooms = DB::select("
                select ht.cod_hotel, 
                       ht.nombre, 
                       ht.ciudad, 
                       hb.cod_habitacion, 
                       hb.identificador,
                       hb.cant_personas,
                       hb.costo_base,
                       hb.tipo_habitacion,
                       hb.impuesto
                from  hotel ht, 
                      habitacion hb
                where hb.cod_hotel = ht.cod_hotel
                  and :cant_personas <= hb.cant_personas
                  and hb.estado = 1
                  and ht.estado = 1
                  and upper(ht.ciudad) = upper(:ciudad)
                  and not exists(select 1
                                   from reserva rs 
                                  where rs.cod_habitacion = hb.cod_habitacion
                                    and rs.estado <> 'finalizada'
                                    and STR_TO_DATE(:fecha_entrada, '%Y-%m-%d') BETWEEN rs.fecha_entrada and rs.fecha_salida
                                    and STR_TO_DATE(:fecha_salida, '%Y-%m-%d') BETWEEN rs.fecha_entrada and rs.fecha_salida)
                ", ['cant_personas' => $request->cant_personas,
                    'ciudad'        => $request->ciudad,
                    'fecha_entrada' => $request->fecha_entrada,
                    'fecha_salida'  => $request->fecha_salida]);

            if (count($rooms) == 0) {
                throw new \InvalidArgumentException('No se encontraron Hoteles/Habitaciones disponibles para los datos ingresados');
            }

            return response()->json([
                'success' => true,
                'rooms'   => $rooms
            ]);
        }catch (\InvalidArgumentException $ex){
            return response()->json([
            'success' => false,
            'message' => $ex->getMessage()
            ], 500);
        }catch (\Exception $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }
    }
}
