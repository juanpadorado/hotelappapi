<?php

namespace App\Http\Controllers;

use App\Hotel;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use JWTAuth;

class HotelController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {
        $hotels = Hotel::all();

        return response()->json([
            'success' => true,
            'hotels' => $hotels
        ]);
    }

    public function show($id)
    {
        $hotel = Hotel::find($id);

        return response()->json([
            'success' => true,
            'hotel' => $hotel
        ]);
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'nombre'     => 'required',
                'ciudad'     => 'required',
                'cant_pisos' => 'required|integer'
            ]);

            $hotel             = new Hotel();
            $hotel->nombre     = $request->nombre;
            $hotel->estado     = 1;
            $hotel->ciudad     = $request->ciudad;
            $hotel->cant_pisos = $request->cant_pisos;

            if ($hotel->save()) {
                return response()->json([
                    'success' => true,
                    'Hotel'   => $hotel,
                    'message' => 'El hotel se ha creado con éxito'
                ]);
            } else {
                throw new \InvalidArgumentException('El hotel no se pudo crear');
            }
        }catch (\InvalidArgumentException $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }catch (ValidationException $ex ) {
            return response()->json([
                'success' => false,
                'message' => $ex->errors()
            ], 400);
        }catch (\Exception $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }
    }

    public function active(Request $request)
    {
        try {
            $hotel = Hotel::find($request->get('cod_hotel'));

            $hotel->estado = $request->get('estado');

            if ($hotel->save()) {
                return response()->json([
                    'success' => true,
                    'message' => 'El estado del hotel se ha actualizado con éxito'
                ]);
            } else {
                throw new \InvalidArgumentException('El hotel no se pudo actualiza');
            }
        }catch (\InvalidArgumentException $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }catch (\Exception $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $hotel = Hotel::find($id);

            $hotel->nombre     = $request->nombre;
            $hotel->ciudad     = $request->ciudad;
            $hotel->cant_pisos = $request->cant_pisos;

            if ($hotel->save()) {
                return response()->json([
                    'success' => true,
                    'hotel'   => $hotel,
                    'message' => 'El hotel se ha actualizado con éxito'
                ]);
            } else {
                throw new \InvalidArgumentException('El hotel no se pudo actualiza');
            }
        }catch (\InvalidArgumentException $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }catch (\Exception $ex){
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ], 500);
        }
    }

}
