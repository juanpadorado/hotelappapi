<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReserveGuest extends Model
{
    const CREATED_AT = 'fec_creacion';
    const UPDATED_AT = 'fec_actualiza';

    protected $table = 'reserva_huesped';
    protected $primaryKey = 'cod_reserva_huesped';

    protected $fillable = [
        'cod_reserva', 'cod_huesped', 'propietario',
    ];

    public function reserve()
    {
        return $this->belongsTo('App\Reserve', 'cod_reserva');
    }

    public function guest()
    {
        return $this->belongsTo('App\Guest', 'cod_huesped');
    }
}
